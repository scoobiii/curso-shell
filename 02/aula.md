
# Aula 1: Revisão

## Links

* https://gitlab.com/blau_araujo/curso-shell
* https://debxp.org/artigos/as_incriveis_funcoes_das_funcoes_em_bash

## Análise do desafio

* Funções no Bash
* Parâmetros posicionais
* Escopo de variáveis
* Comando `declare`
* Desenhando linhas no terminal
* Checando se arquivo existe e é válido
* Trabalhando com modelos
* Utilitário `setsid`

## Além do Bash

* O utilitário `chmod`
* O utilitário `mkdir`
* O utilitário `cat`
* O utilitário `setsid`
* O utilitário `grep`


