#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

# Loop for estilo C...
loopc() for ((n = $1; n <= $2; n+=$3)); do
    echo $n
done

# Loop for com 'in "$@"' implícito...
loopb() for p; do
    echo $p
done

echo
loopb "$@"
echo
# loopc ${1:-0} ${2:-0} ${3:-1}
echo
