#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

# Loop while...
loopw() while [[ ${entrada,,} != 'q' ]]; do
    entrada=''
    read -p "Tecle algo (q para sair): " entrada
done

# Loop until...
loopu() until [[ ${entrada,,} != 'q' ]]; do
    entrada=''
    read -p "Tecle algo diferente de q para sair: " entrada
done

echo
loopw
echo
loopu
echo
