#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

opt() case ${1,,} in
              q) echo; exit ;;
#          [a-z]) echo "Você teclou uma letra..." ;;
    [[:alpha:]]) echo "Você teclou uma letra..." ;;
          [0-9]) echo "Você teclou um dígito..." ;;
    [[:punct:]]) echo "Você teclou um ponto ou um acento..."  ;;
              *) echo "Qualquer coisa..." ;;
esac

while :; do
    read -p "Tecle algo: "
    opt $REPLY
done
