#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

calc() (( resultado = "$@" ))

calc "$@"

echo $resultado
