# curso-shell

Material do curso Além do Bash - Programando no shell do GNU/Linux


## Aula 1

* [Conteúdo da aula](https://gitlab.com/blau_araujo/curso-shell/-/blob/master/01/aula.md)
* [Desafio 1: Crie um script para gerar modelos de scripts](https://gitlab.com/blau_araujo/curso-shell/-/blob/master/desafios/01-criador-de-scripts.md)
* [Lista dos exemplos](https://gitlab.com/blau_araujo/curso-shell/-/blob/master/01/exemplos.txt)


